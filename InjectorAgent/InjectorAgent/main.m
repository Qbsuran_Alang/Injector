//
//  main.m
//  InjectorAgent
//
//  Created by 聲華 陳 on 2015/4/18.
//  Copyright (c) 2015年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "curl/curl.h"
#include <spawn.h>
extern char **environ;
#define AGENT_URL "https://nrl.cce.mcu.edu.tw/injector/Release/agent"
#define VERSION_PATH "/var/root/Documents/InjectorUpdateLog"
#define PREFERENCELOADER @"/var/mobile/Library/Preferences/tw.edu.mcu.cce.nrl.InjectorPreferenceLoader.plist"
#define PREFERENCELOADER_DEFAULT @"/Library/PreferenceBundles/InjectorPreferenceLoader.bundle/InjectorPreferenceLoader.plist"
static NSString *cmd = @"";

static size_t process_data(void *buffer, size_t size, size_t nmemb, void *user_p)
{
    cmd = [cmd stringByAppendingString:[NSString stringWithUTF8String:buffer]];
    
    return nmemb;
}

static NSDictionary *json2dictionary(NSString *json)
{
    NSDictionary *dict = nil;
    
    NSData *data = [json dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error = nil;
    dict = [NSJSONSerialization JSONObjectWithData:data
                                           options:kNilOptions
                                             error:&error];
    if(error) {
        NSLog(@"%@", error.localizedDescription);
        return nil;
    }
    return dict;
}

static size_t downloadFile(void *buffer, size_t size, size_t nmemb, void *user_p)
{
    NSString *filename = (__bridge NSString *)user_p;
    FILE *fp = fopen([filename UTF8String], "a+");
    size_t return_size = fwrite(buffer, size, nmemb, fp);
    fclose(fp);
    return return_size;
}

static int system_no_deprecation(const char *command)
{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
    return system(command);
#pragma GCC diagnostic pop
}

static BOOL checkneedupdate(NSString *log)
{
    FILE *updatelog = fopen(VERSION_PATH, "r+");
    if(!updatelog) {
        updatelog = fopen(VERSION_PATH, "w+"); //can't read, then create
        if(!updatelog)
            return NO;
    }
    char tmp[1024];
    while(1) {
        fgets(tmp, sizeof(tmp) - 1, updatelog);
        strtok(tmp, "\r\n"); //replace newline
        if(!strcmp(tmp, [log UTF8String])) {
            NSLog(@"%@, don't need update", log);
            fclose(updatelog);
            return NO;
        }
        if(feof(updatelog))
            break;
    }
    fclose(updatelog);
    NSLog(@"need update, %@", log);
    return YES;
}

static void recordversion(NSString *log)
{
    FILE *updatelog = fopen(VERSION_PATH, "r+");
    if(!updatelog) {
        updatelog = fopen(VERSION_PATH, "w+"); //can't read, then create
        if(!updatelog)
            return;
    }
    fseek(updatelog, 0, SEEK_END); //set to end
    fprintf(updatelog, "%s\n", [log UTF8String]);
    fclose(updatelog);
}

static id plistObject(NSString *key, int index)
{
    NSDictionary *plist =
    [[NSDictionary alloc] initWithContentsOfFile:PREFERENCELOADER];
    id object = nil;
    
    if(plist && plist.count == 0) {
        plist = [[NSDictionary alloc] initWithContentsOfFile:PREFERENCELOADER_DEFAULT];
        NSArray *items = [plist valueForKey:@"items"];
        object = [items[index] valueForKey:@"default"];
    }
    else {
        object = [plist valueForKey:key];
    }
    return object;
}

int main (int argc, const char * argv[])
{
    NSDictionary *dict = nil;
    CURLcode code = CURLE_OK;
    NSString *event = nil;
    sleep(0);
    
    if((code = curl_global_init(CURL_GLOBAL_SSL)) != CURLE_OK)
        goto BAD;
    
    CURL *curl = curl_easy_init();
    if(curl == NULL)
        goto BAD;
    
    curl_easy_setopt(curl, CURLOPT_URL, AGENT_URL);
    curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1L);
    curl_easy_setopt(curl, CURLOPT_USERAGENT, curl_version());
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &process_data);
    
    if((code = curl_easy_perform(curl)) != CURLE_OK)
        goto BAD;
    
    dict = json2dictionary(cmd);
    if(!dict)
        goto BAD;
    
    event = [dict valueForKey:@"event"];
    printf("Event: %s\n", [event UTF8String]);
    if([event isEqualToString:@"update"]) {
        id object = plistObject(@"SwitchAutoUpdate", 3);
        BOOL switchAutoUpdate = YES;
        if(object != nil)
            switchAutoUpdate = [object boolValue];
        if(!switchAutoUpdate) //don't auto update
            goto OK;
        
        NSDictionary *action = [dict valueForKey:@"action"];
        NSString *version = [action valueForKey:@"version"];
        NSString *type = [action valueForKey:@"type"];
        NSString *url = [action valueForKey:@"url"];
        NSString *filename = [action valueForKey:@"filename"];
        NSArray *cmds = [action valueForKey:@"cmds"];
        NSNumber *interval = [action valueForKey:@"interval"];
        
        if(version == nil || type == nil)
            goto BAD;
        NSString *log = [NSString stringWithFormat:@"%@: %@", type, version];
        BOOL check = checkneedupdate(log);
        if(!check)
            goto BAD;
        
        curl_easy_setopt(curl, CURLOPT_URL, [url UTF8String]);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &downloadFile);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, filename);
        
        //download file
        if((code = curl_easy_perform(curl)) != CURLE_OK)
        goto BAD;
        
        printf("%s updating to version %s, cmds: ", [type UTF8String], [version UTF8String]);
        for(NSString *cmd in cmds) {
            system_no_deprecation([cmd UTF8String]);
            sleep([interval intValue]);
        }
        printf("\n");
        recordversion(log);
    }
    else if([event isEqualToString:@"cmds"]) {
        NSDictionary *action = [dict valueForKey:@"action"];
        NSArray *cmds = [action valueForKey:@"cmds"];
        NSNumber *interval = [action valueForKey:@"interval"];
        for(NSString *cmd in cmds) {
            system_no_deprecation([cmd UTF8String]);
            sleep([interval intValue]);
        }
    }
    
OK:
    curl_global_cleanup();
    printf("Bye\n");
	return 0;
BAD:
    NSLog(@"%s", curl_easy_strerror(code));
    curl_global_cleanup();
    return -1;
}

