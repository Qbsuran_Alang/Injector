//
//  IJTSysctl.h
//  IJTSysctl
//
//  Created by 聲華 陳 on 2015/8/1.
//
//

#import <Foundation/Foundation.h>

@interface IJTSysctl : NSObject

/**
 * 取得參數
 * 成功傳回數值, 失敗傳回-1
 */
+ (int)sysctlValueByname: (NSString *)name;
@end
