//
//  main.m
//  InjectorARPDeamon
//
//  Created by 聲華 陳 on 2015/11/10.
//  Copyright (c) 2015年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IJTARPDefender.h"
int main (int argc, const char * argv[])
{

    @autoreleasepool
    {
        //because when booting, network connection is not available immediately
        if(!(argc >= 2 && !strcmp(argv[1], "skip")))
            sleep(10);
        
        IJTARPDefender *denfender = [[IJTARPDefender alloc] init];
        [denfender defense];
    }
	return 0;
}

