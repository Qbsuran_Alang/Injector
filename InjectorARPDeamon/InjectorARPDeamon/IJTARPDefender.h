//
//  IJTARPDefender.h
//  InjectorARPDeamon
//
//  Created by 聲華 陳 on 2015/11/10.
//
//

#import <Foundation/Foundation.h>

@interface IJTARPDefender : NSObject

- (void)defense;

@end
