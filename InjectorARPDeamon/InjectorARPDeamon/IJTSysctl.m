//
//  IJTSysctl.m
//  IJTSysctl
//
//  Created by 聲華 陳 on 2015/8/1.
//
//

#import "IJTSysctl.h"
#include <sys/sysctl.h>

@implementation IJTSysctl

+ (int)sysctlValueByname: (NSString *)name {
    int buf = -1;
    size_t size = sizeof(buf);
    if(sysctlbyname([name UTF8String], &buf, &size, NULL, 0) == -1)
        return -1;
    return buf;
}

@end
