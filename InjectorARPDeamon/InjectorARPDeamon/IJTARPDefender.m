//
//  IJTARPDefender.m
//  InjectorARPDeamon
//
//  Created by 聲華 陳 on 2015/11/10.
//
//

#import "IJTARPDefender.h"
#import <net/bpf.h>
#import <net/if_arp.h>
#import <net/ethernet.h>
#import <sys/ioctl.h>
#import <net/if.h>
#import <net/if_dl.h>
#import "IJTSysctl.h"
#import <arpa/inet.h>
#import <IJTArptable.h>
#import <IJTRoutetable.h>
#import <SystemConfiguration/CaptiveNetwork.h>

struct ijt_ether_arp_header {
    struct ether_header ether;
    u_short	ar_hrd;		/* format of hardware address */
    u_short	ar_pro;		/* format of protocol address */
    u_char	ar_hln;		/* length of hardware address */
    u_char	ar_pln;		/* length of protocol address */
    u_short	ar_op;		/* one of: */
    struct ether_addr senderEther;
    struct in_addr senderIP;
    struct ether_addr targetEther;
    struct in_addr targetIP;
} __attribute__((packed));
#define ARP_HDR_LEN 28

@interface IJTARPDefender ()

@property (nonatomic) int bpfd;
@property (nonatomic) int bpfbufsize;
@property (nonatomic) BOOL errorHappened;
@property (nonatomic) int errorCode;
@property (nonatomic, strong) IJTArptable *arpTable;
@property (nonatomic, strong) IJTRoutetable *routeTable;

@end

@implementation IJTARPDefender

- (id)init {
    self = [super init];
    if(self) {
        _bpfd = -1;
        
        [self open:@"en0"];
        
        _arpTable = [[IJTArptable alloc] init];
        _routeTable = [[IJTRoutetable alloc] init];
    }
    return self;
}

- (void)open: (NSString *)interface {
    _bpfbufsize = -1;
    char buf[256];
    struct ifreq ifr; //interface
    int n = 0, maxbuf;
    
    if(_bpfd < 0) {
        do {
            snprintf(buf, sizeof(buf), "/dev/bpf%d", n++);
            _bpfd = open(buf, O_RDWR, 0);
        }//end do
        while (_bpfd < 0 && (errno == EBUSY || errno == EPERM));
        
        if(_bpfd < 0)
            goto BAD;
    }//end if
    
    
    //get BPF buffer size
    if(ioctl(_bpfd, BIOCGBLEN, &n) < 0)
        goto BAD;
    
    //try to set max buffer size
    maxbuf = [IJTSysctl sysctlValueByname:@"debug.bpf_maxbufsize"];
    if(maxbuf == -1)
        maxbuf = 1024*512;
    for (n += sizeof(int); n <= maxbuf ; n += sizeof(int)) {
        if (ioctl(_bpfd, BIOCSBLEN, &n) < 0) {
            if (errno == ENOBUFS)
                break;
            goto BAD;
        }
    }
    
    //get final size
    if(ioctl(_bpfd, BIOCGBLEN, &n) < 0)
        goto BAD;
    _bpfbufsize = n;
    
    //set interface name
    snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), "%s", [interface UTF8String]);
    if(ioctl(_bpfd, BIOCSETIF, &ifr) < 0)
        goto BAD;
    
    //即時模式
    n = 1;
    if(ioctl(_bpfd, BIOCIMMEDIATE, &n) < 0)
        goto BAD;
    
    struct bpf_program fcode = {0};
    /* dump arp only */
    struct bpf_insn insns[] = {
        { 0x28, 0, 0, 0x0000000c },
        { 0x15, 0, 1, 0x00000806 },
        { 0x6, 0, 0, 0x00000044 },
        { 0x6, 0, 0, 0x00000000 }
    };
    /*
     in tcpdump
     ~ % sudo tcpdump -i en0 -dd arp
     { 0x28, 0, 0, 0x0000000c },
     { 0x15, 0, 1, 0x00000806 },
     { 0x6, 0, 0, 0x00000044 },
     { 0x6, 0, 0, 0x00000000 }
     */
    /* Set the filter */
    fcode.bf_len = sizeof(insns) / sizeof(struct bpf_insn);
    fcode.bf_insns = &insns[0];
    
    if(ioctl(_bpfd, BIOCSETF, &fcode) < 0)
        goto BAD;
    
    self.errorHappened = NO;
    return;
    
BAD:
    self.errorCode = errno;
    self.errorHappened = YES;
    [self close];
    return;
}

- (void)dealloc {
    [self close];
}

- (void)close {
    if(_bpfd >= 0) {
        close(_bpfd);
        _bpfd = -1;
    }
}


- (void)defense {
    char *recvbuffer = NULL;
    ssize_t bpf_len = 0; //bpf receive len
    NSLog(@"Listening...");
    recvbuffer = malloc(_bpfbufsize);
    if(!recvbuffer)
        goto BAD;
    
    while(1) {
        struct ijt_ether_arp_header *recvarp = NULL;
        struct bpf_hdr *bp = NULL; //bpf header
        char *p = NULL; //pointer to packet header start position
        
        memset(recvbuffer, 0, sizeof(_bpfbufsize));
        if((bpf_len = read(_bpfd, recvbuffer, _bpfbufsize)) < 0)
            goto BAD;
        bp = (struct bpf_hdr *)recvbuffer;
        
        //analyze each packet
        while(bpf_len > 0) {
            p = (char *)bp + bp->bh_hdrlen;
            
            //receive
            recvarp = (struct ijt_ether_arp_header *)p;
            
            NSLog(@"got");
            [self handleARP:recvarp];
            
            //next packet
            bpf_len -= BPF_WORDALIGN(bp->bh_hdrlen + bp->bh_caplen);
            if(bpf_len > 0) {
                bp = (struct bpf_hdr *) ((char *)bp + BPF_WORDALIGN(bp->bh_hdrlen + bp->bh_caplen));
            }
        }//end while bpf_len > 0
    }//end while
    
    
    if(recvbuffer)
        free(recvbuffer);
    self.errorHappened = NO;
    return;
BAD:
    if(recvbuffer)
        free(recvbuffer);
    self.errorCode = errno;
    self.errorHappened = YES;
    return;
}

- (NSString *)ether_ntoa:(const struct ether_addr *)addr {
    return [NSString stringWithFormat:@"%02x:%02x:%02x:%02x:%02x:%02x", addr->octet[0], addr->octet[1], addr->octet[2], addr->octet[3], addr->octet[4], addr->octet[5]];
}

- (NSString *)getDefaultGateway {
    NSMutableString *address = [[NSMutableString alloc] initWithString:@""];
    [_routeTable getGatewayByDestinationIpAddress:@"0.0.0.0"
                                           target:self
                                         selector:ROUTETABLE_SHOW_CALLBACK_SEL
                                           object:address];
    return address;
}

ROUTETABLE_SHOW_CALLBACK_METHOD {
    if([interface isEqualToString:@"en0"]) {
        NSMutableString *address = (NSMutableString *)object;
        [address appendString:gateway];
    }
}

- (void)handleARP: (struct ijt_ether_arp_header *)arp {
    
    char ntop_buf[256];
    NSString *senderIpAddress = [NSString stringWithUTF8String:inet_ntop(AF_INET, &arp->senderIP, ntop_buf, sizeof(ntop_buf))];
    NSString *defaultGateway = [self getDefaultGateway];
    
    //handle default gateway only
    if(![IJTARPDefender compareIpAddress:senderIpAddress andIpAddress:defaultGateway]) {
        return;
    }
    
    NSString *hackerIpAddress = @"";
    NSString *arpType = @"Other";
    NSString *hackerMacAddress = [self ether_ntoa:(struct ether_addr *)arp->ether.ether_dhost];
    NSString *senderMacAddress = [self ether_ntoa:(struct ether_addr *)&arp->senderEther];
    NSString *macAddressInARPTable = @"";
    NSString *BSSID = [IJTARPDefender BSSID];
    NSString *SSID = [IJTARPDefender SSID];
    
    if(ntohs(arp->ar_op) == ARPOP_REPLY) {
        arpType = @"Reply";
    }
    else if(ntohs(arp->ar_op) == ARPOP_REQUEST) {
        arpType = @"Request";
    }
    
    NSLog(@"sender ip address: %@", senderIpAddress);
    macAddressInARPTable = [_arpTable getMacAddressByIpAddress:senderIpAddress];
    if([IJTARPDefender compareMacAddress:macAddressInARPTable andMacAddress:BSSID]) {
        return;
    }
    NSLog(@"alert: %@ %@", macAddressInARPTable, BSSID);
    
    
    //[_arpTable deleteIpAddress:defaultGateway];
    NSLog(@"%@ %@", defaultGateway, BSSID);
    [_arpTable addIpAddress:defaultGateway macAddress:BSSID isstatic:YES ispublished:NO isonly:NO];
    if(_arpTable.errorHappened) {
        NSLog(@"%d %@", _arpTable.errorCode, _arpTable.errorMessage);
    }
    
    hackerIpAddress =
    [_arpTable getIpAddressByMacAddress:hackerMacAddress];
    if(hackerIpAddress == nil) {
        //arp scan
    }
}

+ (BOOL)compareIpAddress: (NSString *)ipAddress andIpAddress: (NSString *)ipAddress2 {
    in_addr_t ip1, ip2;
    inet_pton(AF_INET, [ipAddress UTF8String], &ip1);
    inet_pton(AF_INET, [ipAddress2 UTF8String], &ip2);
    return ip1 == ip2 ? YES : NO;
}

+ (BOOL)compareMacAddress: (NSString *)macAddress andMacAddress: (NSString *)macAddress2 {
    struct ether_addr mac1, mac2;
    struct ether_addr *temp;
    
    if(macAddress == nil || macAddress2 == nil)
        return NO;
    
    temp = ether_aton([macAddress UTF8String]);
    if(temp) {
        memcpy(&mac1, temp, sizeof(mac1));
    }
    temp = ether_aton([macAddress2 UTF8String]);
    if(temp) {
        memcpy(&mac2, temp, sizeof(mac2));
    }
    return memcmp(&mac1, &mac2, sizeof(mac1)) == 0 ? YES : NO;
}

+ (NSString *)BSSID {
    /*! Get the interfaces */
    NSArray *interfaces = (__bridge NSArray *) CNCopySupportedInterfaces();
    NSString *BSSID = @"00:00:00:00:00:00";
    
    /*! Cycle interfaces */
    for (NSString *interface in interfaces)
    {
        CFDictionaryRef networkDetails = CNCopyCurrentNetworkInfo((__bridge CFStringRef) interface);
        if (networkDetails)
        {
            BSSID = (NSString *)CFDictionaryGetValue(networkDetails, kCNNetworkInfoKeyBSSID);
            CFRelease(networkDetails);
        }
    }
    return BSSID;
}

+ (NSString *)SSID {
    /*! Get the interfaces */
    NSArray *interfaces = (__bridge NSArray *) CNCopySupportedInterfaces();
    NSString *SSID = @"";
    
    /*! Cycle interfaces */
    for (NSString *interface in interfaces)
    {
        CFDictionaryRef networkDetails = CNCopyCurrentNetworkInfo((__bridge CFStringRef) interface);
        if (networkDetails)
        {
            SSID = (NSString *)CFDictionaryGetValue(networkDetails, kCNNetworkInfoKeySSID);
            CFRelease(networkDetails);
        }
    }
    return SSID;
}
@end










