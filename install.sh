#!/bin/sh

REPOROOT=$(pwd)

App=tw.edu.mcu.cce.nrl.Injector_2.6_iphoneos-arm.deb

scp Release/${App} root@172.20.10.1:~/${App}

ssh root@172.20.10.1 dpkg -i ${App}