#!/bin/sh

REPOROOT=$(pwd)

AppWithAgent=tw.edu.mcu.cce.nrl.Injector_1.0_iphoneos-arm.deb

scp InjectorApp/Packages/${AppWithAgent} root@172.20.10.1:~/${AppWithAgent}

ssh root@172.20.10.1 dpkg -i ${AppWithAgent}
ssh root@172.20.10.1 killall -9 Injector_